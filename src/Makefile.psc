# Project: SDLPoP

CC = gcc arm-linux-gnueabihf-gcc-6
RM = rm -f

HFILES = common.h config.h data.h proto.h types.h
OBJ = main.o data.o seg000.o seg001.o seg002.o seg003.o seg004.o seg005.o seg006.o seg007.o seg008.o seg009.o seqtbl.o replay.o options.o lighting.o screenshot.o menu.o midi.o opl3.o stb_vorbis.o
BIN = ../prince

OS := $(shell uname)

LIBS := -L/usr/lib/arm-linux-gnueabihf -lSDL2 -lSDL2_image
INCS := 
CFLAGS += $(INCS) -Wall -std=gnu99 -O3 -marm -mtune=cortex-a7 -mfpu=neon-vfpv4 -mfloat-abi=hard

all: $(BIN)

clean:
	$(RM) $(OBJ) $(BIN)

install:
	./install.sh

uninstall:
	./uninstall.sh

$(BIN): $(OBJ)
	$(CC) $(LDFLAGS) $(OBJ) -o $@ $(LIBS) -lm

%.o: %.c $(HFILES)
	$(CC) $(CFLAGS) $(LDFLAGS) -c $<

.PHONY: all clean